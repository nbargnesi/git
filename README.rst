=============
nbargnesi/git
=============

What
----

A fork of Git_ used to place entire Linux installs under version control.


Why
---

(as in, "Why the fork?")

Upstream Git is unsuitable for this type of use. If I checkout a modified file
that Git is tracking, it is unlinked_ and recreated using the umask_ of the
current user. I wanted the file to get truncated and rewritten, preserving its
metadata.

To see this in action, change the owning group on a file Git is tracking, make
a modification, and check it out (effectively reverting the modification)::

    # ls -l
    4.0K -rw-r--r-- 1 nick nick 2 Feb  8 13:15 test.txt
    # chgrp users test.txt
    # ls -l
    4.0K -rw-r--r-- 1 nick users 1 Feb  8 13:18 test.txt
    # echo > test.txt
    # git ls-files --modified
    test.txt
    # git checkout test.txt
    # ls -l
    4.0K -rw-r--r-- 1 nick nick 2 Feb  8 13:20 test.txt


How
---

One patch_ was all I needed to get 80% of the functionality I was looking for.
In the previous example, Git instead truncates ``test.txt`` rather than
removing it, then writes its contents. Metadata is preserved.

.. _Git: https://github.com/git/git
.. _unlinked: http://en.wikipedia.org/wiki/Unlink_(Unix)
.. _umask: http://en.wikipedia.org/wiki/Umask
.. _patch: https://bitbucket.org/nbargnesi/git/commits/5af95c97f74cbf6fa67081458ec28d3026d1298a

